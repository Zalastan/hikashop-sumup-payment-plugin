<div class="hikashop_example_end" id="hikashop_example_end">
    <div class="uk-alert uk-alert-info">
        <p><?php echo JText::_('SUMUP_ORDER_CREATED_FULLFILL_FORM_FOR_PAYMENT'); ?></p>
    </div>
    <div id="sumup-card" class="uk-form uk-background-muted"></div>
    <script type="text/javascript" src="https://gateway.sumup.com/gateway/ecom/card/v2/sdk.js"></script>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            SumUpCard.mount({
                checkoutId: '<?php echo $this->vars['checkoutId']; ?>',
                locale: 'fr-FR',
                showFooter: true,
                amount: '<?php echo $this->vars['amount']; ?>',
                currency: '<?php echo $this->vars['currency']; ?>',
                installments: null,
                onResponse: function (type, body) {
                    switch (body.status) {
                        case 'FAILED':
                            document.getElementById("sumup-card").innerHTML = `
                                <div class="uk-alert-danger" uk-alert>
                                    <a class="uk-alert-close" uk-close></a>
                                    <p><?php echo JText::_('SUMUP_FAILED_PAYMENT'); ?></p>
                                    <p><?php echo JText::_('SUMUP_FAILED_PAYMENT_CANCEL'); ?></p>
                                </div>
                            `;
                            document.getElementById("sumup-card").removeAttribute("style");
                            break;

                        case 'PAID':
                            document.getElementById("sumup-card").innerHTML = `
                                <div class="uk-alert-success" uk-alert>
                                    <a class="uk-alert-close" uk-close></a>
                                    <p><?php echo JText::_('SUMUP_PAID_PAYMENT'); ?></p>
                                </div>
                            `;
                            document.getElementById("sumup-card").removeAttribute("style");
                            const url = "<?php echo $this->vars['notify_url']; ?>";
                            const sendData = {
                                'payment_status': body.status,
                                'payment_transaction_code': body.transactions[0].transaction_code,
                                'payment_transaction_id': body.transactions[0].id,
                                'order_id': <?php echo $this->vars['order_id']; ?>,
                                'payment_amount': body.amount,
                                'payment_currency': body.currency,
                                'order_number': body.checkout_reference
                            };
                            let formBody = [];
                            for (let property in sendData) {
                                const encodedKey = encodeURIComponent(property);
                                const encodedValue = encodeURIComponent(sendData[property]);
                                formBody.push(encodedKey + "=" + encodedValue);
                            }
                            formBody = formBody.join("&");
                            const xhr = new XMLHttpRequest();
                            xhr.open("POST", url, true);
                            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            xhr.send(formBody);
                            break;
                    }
                }
            });
        });
    </script>
</div>