<?php

require 'vendor/autoload.php';

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use SumUp\SumUp;

/*
* A payment plugin called "sumup". This is the main file of the plugin.
*/

// You need to extend from the hikashopPaymentPlugin class which already define lots of functions in order to simplify your work
class plgHikashoppaymentSumup extends hikashopPaymentPlugin
{
    protected $autoloadLanguage = true;
    //var $accepted_currencies = array("EUR"); //List of the plugin's accepted currencies. The plugin won't appear on the checkout if the current currency is not in that list. You can remove that attribute if you want your payment plugin to display for all the currencies
    var $multiple = true; // Multiple plugin configurations. It should usually be set to true
    var $name = 'sumup'; //Payment plugin name (the name of the PHP file)
    // This array contains the specific configuration needed (Back end > payment plugin edition), depending of the plugin requirements.
    // They will vary based on your needs for the integration with your payment gateway.
    // The first parameter is the name of the field. In upper case for a translation key.
    // The available types (second parameter) are: input (an input field), html (when you want to display some custom HTML to the shop owner), textarea (when you want the shop owner to write a bit more than in an input field), big-textarea (when you want the shop owner to write a lot more than in an input field), boolean (for a yes/no choice), checkbox (for checkbox selection), list (for dropdown selection) , orderstatus (to be able to select between the available order statuses)
    // The third parameter is the default value.
    var $pluginConfig = array(
        'id' => array("Id", 'input'),
        'client_id' => array("Client id", 'input'),
        'client_secret' => array("Client secret", 'input'),
        'pay_to_email' => array("SUMUP_PAY_TO_EMAIL", 'input'),
        'notification' => array('ALLOW_NOTIFICATIONS_FROM_X', 'boolean', '1'), //To allow (or not) notifications from the payment platform. The plugin can only work if notifications are allowed
        'payment_url' => array("Payment URL", 'input'), // Platform payment's url
        'debug' => array('DEBUG', 'boolean', '0'), //Write some things on the debug file
        'cancel_url' => array('CANCEL_URL_DEFINE', 'html', ''), //The URL where the user is redirected after a fail during the payment process
        'return_url_gateway' => array('RETURN_URL_DEFINE', 'html', ''), // The URL where the user is redirected after the payment is done on the payment gateway. It's a pre determined URL that has to be given to the payment gateway
        'return_url' => array('RETURN_URL', 'input'), //The URL where the user is redirected by HikaShop after the payment is done ; "Thank you for purchase" page
        'notify_url' => array('NOTIFY_URL_DEFINE', 'html', ''), //The URL where the payment plateform the user about the payment (fail or success)
        'invalid_status' => array('INVALID_STATUS', 'orderstatus'), //Invalid status for order in case of problem during the payment process
        'verified_status' => array('VERIFIED_STATUS', 'orderstatus') //Valid status for order if the payment has been done well
    );

    // The constructor is optional if you don't need to initialize some parameters of some fields of the configuration and not that it can also be done in the getPaymentDefaultValues function as you will see later on
    function __construct(&$subject, $config)
    {
        $this->pluginConfig['notification'][0] = JText::sprintf('ALLOW_NOTIFICATIONS_FROM_X', 'Sumup');
        // This is the cancel URL of HikaShop that should be given to the payment gateway so that it can redirect to it when the user cancel the payment on the payment gateway page. That URL will automatically cancel the order of the user and redirect him to the checkout so that he can choose another payment method
        $this->pluginConfig['cancel_url'][2] = HIKASHOP_LIVE . "index.php?option=com_hikashop&ctrl=order&task=cancel_order";
        // This is the "thank you" or "return" URL of HikaShop that should be given to the payment gateway so that it can redirect to it when the payment of the user is valid. That URL will reinit some variables in the session like the cart and will then automatically redirect to the "return_url" parameter
        $this->pluginConfig['return_url'][2] = HIKASHOP_LIVE . "index.php?option=com_hikashop&ctrl=checkout&task=after_end";
        // This is the "notification" URL of HikaShop that should be given to the payment gateway so that it can send a request to that URL in order to tell HikaShop that the payment has been done (sometimes the payment gateway doesn't do that and passes the information to the return URL, in which case you need to use that notification URL as return URL and redirect the user to the HikaShop return URL at the end of the onPaymentNotification function)
        $this->pluginConfig['notify_url'][2] = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=checkout&task=notify&amp;notif_payment=' . $this->name . '&tmpl=component';

        return parent::__construct($subject, $config);
    }


    //This function is called at the end of the checkout. That's the function which should display your payment gateway redirection form with the data from HikaShop

    /**
     * @throws \SumUp\Exceptions\SumUpResponseException
     * @throws \SumUp\Exceptions\SumUpConnectionException
     * @throws \SumUp\Exceptions\SumUpArgumentException
     * @throws \SumUp\Exceptions\SumUpAuthenticationException
     * @throws \SumUp\Exceptions\SumUpSDKException
     */
    function onAfterOrderConfirm(&$order, &$methods, $method_id)
    {
        parent::onAfterOrderConfirm($order, $methods, $method_id); // This is a mandatory line in order to initialize the attributes of the payment method

        $notify_url = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=checkout&task=notify&notif_payment=' . $this->name . '&tmpl=component&lang=' . $this->locale . $this->url_itemid;
        $return_url = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=checkout&task=after_end&order_id=' . $order->order_id . $this->url_itemid;
        $cancel_url = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=order&task=cancel_order&order_id=' . $order->order_id . $this->url_itemid;
        //Here we can do some checks on the options of the payment method and make sure that every required parameter is set and otherwise display an error message to the user
        if (empty($this->payment_params->id)) //The plugin can only work if those parameters are configured on the website's backend
        {
            $this->app->enqueueMessage('You have to configure an id for the Sumup plugin payment first : check your plugin\'s parameters, on your website backend', 'error');

            //Enqueued messages will appear to the user, as Joomla's error messages
            return false;
        }
        elseif (empty($this->payment_params->client_id))
        {
            $this->app->enqueueMessage('You have to configure a client_id for the Sumup plugin payment first : check your plugin\'s parameters, on your website backend', 'error');

            return false;
        }
        elseif (empty($this->payment_params->client_secret))
        {
            $this->app->enqueueMessage('You have to configure a client_secret for the Sumup plugin payment first : check your plugin\'s parameters, on your website backend', 'error');

            return false;
        }
        elseif (empty($this->payment_params->payment_url))
        {
            $this->app->enqueueMessage('You have to configure a payment url for the Sumup plugin payment first : check your plugin\'s parameters, on your website backend', 'error');

            return false;
        }
        else
        {

            $amount = round($order->cart->full_total->prices[0]->price_value_with_tax, 2);
            $payToEmail = $this->payment_params->pay_to_email;
            $currency = $order->order_currency_info->currency_code;
            $checkoutRef = $order->order_number;

            $sumup = new SumUp([
                'app_id' => $this->payment_params->client_id,
                'app_secret' => $this->payment_params->client_secret,
                'grant_type' => 'client_credentials',
                'scopes' => ['payments']
            ]);
            $accessToken = $sumup->getAccessToken();
            $token = $accessToken->getValue();

            $checkoutsService = $sumup->getCheckoutService($accessToken);
            $description = JText::_('SUMUP_ORDER') . ' ' . $order->order_number . ' - ' . $_SERVER['HTTP_HOST'];
            $checkout = $checkoutsService->create($amount, $currency, $checkoutRef, $payToEmail, $description, $payFromEmail = null, $notify_url);
            $checkoutId = $checkout->getBody()->id;
            $vars = array(
                'checkoutId' => $checkoutId,
                'amount' => $amount,
                'currency' => $currency,
                'token' => $token,
                'return' => $return_url,
                'notify_url' => $notify_url,
                'cancel_return' => $cancel_url,
                'order_number' => $order->order_number,
                'order_id' => $order->order_id,
                'description' => $description
            );

            $this->vars = $vars;
            //Ending the checkout, ready to be redirect to the plateform payment final form
            //The showPage function will call the example_end.php file which will display the redirection form
            // containing all the parameters for the payment platform
            return $this->showPage('end');
        }
    }


    //To set the specific configuration (back end) default values (see $pluginConfig array)
    function getPaymentDefaultValues(&$element)
    {
        $element->payment_name = 'Sumup';
        $element->payment_description = 'You can pay by credit card using this Sumup method';
        $element->payment_images = 'MasterCard,VISA,Credit_card,American_Express';
        $element->payment_params->address_type = "billing";
        $element->payment_params->notification = 1;
        $element->payment_params->invalid_status = 'cancelled';
        $element->payment_params->verified_status = 'confirmed';
    }


    //After submiting the plateform payment form, this is where the website will receive the response information from
    // the payment gateway servers and then validate or not the order
    function onPaymentNotification(&$statuses)
    {
        $app = JFactory::getApplication();
        $postData = $app->input->post;
        $payment = new stdClass;
        $payment->status = $postData->get('payment_status');
        $payment->transaction_id = $postData->get('payment_transaction_id');
        $payment->transaction_code = $postData->get('payment_transaction_code');
        $payment->amount = $postData->get('payment_amount');
        $payment->currency = $postData->get('payment_currency');
        $order_id = $postData->get('order_id');

        $dbOrder = $this->getOrder($order_id);
        $this->loadPaymentParams($dbOrder);
        if (empty($this->payment_params))
            return false;
        $this->loadOrderData($dbOrder);

        //Configure the "succes URL" and the "fail URL" to redirect the user if necessary (not necessary for our example platform
        $return_url = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=checkout&task=after_end&order_id=' . $order_id . $this->url_itemid;
        $cancel_url = HIKASHOP_LIVE . 'index.php?option=com_hikashop&ctrl=order&task=cancel_order&order_id=' . $order_id . $this->url_itemid;
        // Note : there is no Debug mode with Sumup sdk
        if ($this->payment_params->debug) //Debug mode activated or not
        {
            //Here we display debug information which will be catched by HikaShop and stored in the payment log file
            // available in the configuration's Files section.
            echo print_r($vars, true) . "\n\n\n";
            echo print_r($dbOrder, true) . "\n\n\n";
            echo print_r($hash, true) . "\n\n\n";
        }

        //Confirm or not the Order, depending of the information received
        if ($payment->status == "PAID")
        {

            $email = new stdClass();
            $email->subject = JText::sprintf('PAYMENT_NOTIFICATION_FOR_ORDER', 'Sumup', 'PAID', $dbOrder->order_number);
            $email->body = str_replace(
                    '<br/>',
                    "\r\n",
                    JText::sprintf('PAYMENT_NOTIFICATION_STATUS', 'Sumup', 'PAID')) . ' ' . JText::sprintf('ORDER_STATUS_CHANGED',
                    hikashop_orderStatus($order_status)) . "\r\n\r\n";

            $this->writeToLog('Sumup transaction code: ' . $payment->transaction_code . ', Sumup transaction id: ' . $payment->transaction_id . ', Payment status: ' . $payment->status);
            $history = new stdClass();
            $history->notified = 1;
            $history->amount = $payment->amount . ' ' . $payment->currency;
            $history->data = $this->writeToLog();
            $this->modifyOrder($order_id, $this->payment_params->verified_status, $history, $email);

            $this->app->redirect($return_url);

            return $return_url;
        }
        else
        {
            $this->writeToLog('Sumup transaction code: ' . $payment->transaction_code . ', Sumup transaction id: ' . $payment->transaction_id . ', Payment status: ' . $payment->status);
            $history = new stdClass();
            $history->notified = 0;
            $history->amount = $payment->amount . ' ' . $payment->currency;
            $history->data = $this->writeToLog();
            $this->modifyOrder($order_id, $this->payment_params->invalid_status, $history, $email);
            $this->app->redirect($cancel_url);

            return $cancel_url;
        }
    }

    function writeToLog($data = null)
    {
        if (!empty($data))
        {
            hikashop_writeToLog($data, $this->name);
            if (is_array($data) || is_object($data))
                $data = str_replace(array("\r", "\n", "\r\n"), "\r\n", print_r($data, true)) . "\r\n\r\n";
            $this->cachedDebug .= $data;
        }

        return $this->cachedDebug;
    }

}
