Copyright (c) Sébastien Gamarde for hikashop plugin payment, HIKARI SOFTWARE for examples files, SumUp Payments Limited
for SDK, Nils Adermann, Jordi Boggiano for composer autoloader.

Before reading this licence, ensure you're right with SumUp Payments
Limited [licence](vendor/sumup/sumup-ecom-php-sdk/LICENSE.md).

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Copyright (c) Sébastien Gamarde pour le plugin de paiement sumup, HIKARI SOFTWARE pour les fichiers d'exemple, SumUp
Payments Limited pour le SDK, Nils Adermann, Jordi Boggiano pour l'autoloader de composer.

Avant de lire cette licence, assurez-vous que vous êtes bien en accord avec
la [licence](./vendor/sumup/sumup-ecom-php-sdk/LICENSE.md) SumUp Payments Limited SumUp Payments Limited.

La permission est accordée par la présente, gratuitement, à toute personne obtenant une copie de ce logiciel et des
fichiers de documentation associés (le "Logiciel"), de traiter le Logiciel sans restriction, y compris sans limitation
d'utilisation, de copier, de modifier, de fusionner, de publier, de distribuer, d'accorder une sous-licence et/ou de
vendre des copies du logiciel, et de permettre aux personnes à qui le logiciel est fourni d'utiliser le même logiciel.
les personnes à qui le logiciel est fourni à le faire, sous réserve des conditions suivantes :

L'avis de droit d'auteur ci-dessus et cet avis d'autorisation doivent être inclus dans toutes les copies ou parties
substantielles du logiciel.

LE LOGICIEL EST FOURNI " EN L'ÉTAT ", SANS GARANTIE D'AUCUNE SORTE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y
LIMITER, LES GARANTIES DE QUALITÉ MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER ET DE SÉCURITÉ. GARANTIES DE QUALITÉ
MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER ET DE NON-VIOLATION. EN AUCUN CAS, LES AUTEURS OU LES DÉTENTEURS DE EN
AUCUN CAS LES AUTEURS OU LES DÉTENTEURS DE DROITS D'AUTEUR NE PEUVENT ÊTRE TENUS RESPONSABLES DE TOUTE RÉCLAMATION, DE
TOUT DOMMAGE OU DE TOUTE AUTRE CONTRACTUELLE, DÉLICTUELLE OU AUTRE, DÉCOULANT DE OU EN RELATION AVEC LE LOGICIEL OU
L'UTILISATION OU AUTRES TRANSACTIONS DU LOGICIEL.