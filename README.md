# Plugin de paiement sumup pour hikashop
**Attention : Veuillez lire ce fichier en entier avant toute demande**
## Installation

### Prérequis

Si vous êtes à l'aise avec l'anglais suivez le lien suivant, il sera sans doute plus à jour que ma documentation.

https://developer.sumup.com/docs/online-payments/introduction/register-app/

Sinon suivez ces instructions :

1. Rendez-vous sur la page https://me.sumup.com/ et connectez vous à votre tableau de bord
2. Cliquez tout en haut à droite sur votre nom d'utilisateur et choisissez parmi les choix de la liste déroulante le lien "Développeurs"
3. Allez dans la section "Écran d'autorisation" et remplissez les champs. Puis, sauvegardez.
4. Créer les identifiants de connexion du client OAuth.
5. Un bouton "JSON" apparaît. Celui-ci contiendra les données de connexion à renseigner dans la configuration du plugin de paiement sumup

### Installation du plugin sur votre boutique hikashop

1. [Téléchargez le plugin](https://gitlab.com/Zalastan/hikashop-sumup-payment-plugin/-/releases) dans sa dernière version de préférence (sauf si le readme le contre-indique)
2. Connectez-vous à votre panneau d'administration
3. Allez dans l'onglet système, puis dans la rubrique installation cliquez sur "Extensions" (Il se peut que sur joomla 3 ce soit un peu différent, mais le procédé est le même ...) Dans le doute voici l'url : /administrator/index.php?option=com_installer&view=install
4. Choisissez l'onglet "Archive à envoyer" et déposer le fichier .zip.


## Configuration du plugin

1. Allez dans Composants -> Hikashop -> Configuration générale.
2. Cliquez sur l'onglet "système" -> modes de paiement et choisissez Hikashop Sumup Payment Plugin
3. Remplissez les champs avec les données de votre fichier JSON de l'étape 5 du prérequis.![](docs/images/Screenshot_20221011_212801.png)


## Activation des paiements auprès de SumUp

**Attention le scope de paiement SumUp est restreint de base. Cela signifie que même avec ce plugin de paiement sumup vous ne pourrez pas payer sans avoir demandé l'activation de ce scope via le support de sumup.**

**https://developer.sumup.com/docs/online-payments/introduction/authorization/#authorization-scopes**


## Tests

Il n'existe pas de sandbox sans demander explicitement de compte spécifique au support SumUp.
https://developer.sumup.com/docs/online-payments/guides/single-payment/#before-you-begin


## Copyright (c)

Voir le fichier [LICENSE](LICENSE.md)

## Screenshots
![](docs/images/plugin-paiement-sumup.png)
![](docs/images/Screenshot_20221011_212801.png)
![](docs/images/Screenshot_20221011_204416.png)
![](docs/images/Screenshot_20221011_220816.png)

## Don
[Faire un don](https://www.paypal.com/donate/?business=M8NUTCUT62GXU&no_recurring=0&item_name=Valorisez+mon+temps+pass%C3%A9+sur+le+d%C3%A9veloppement+de+plugins%2C+modules%2C+composants+que+je+vous+partage+%C3%A0+titre+gracieux.&currency_code=EUR)

En faisant un don, vous valorisez mon temps passé sur le développement de plugins, modules, composants que je vous partage à titre gracieux.